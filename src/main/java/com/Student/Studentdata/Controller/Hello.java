package com.Student.Studentdata.Controller;

import javax.websocket.server.PathParam;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hai")
public class Hello {
    @GetMapping("/welcome")
	public String firstMethod() {
		return "Hello World sound";
		
		
	}
    @PostMapping
    public String secondMethod(String name) {
		return "Name:"+name;
    	
    }
    //path param
//    @GetMapping("/{name}/{id}")
//   // @ResponseBody
//    public String studentData(@PathVariable String name,
//    		@PathVariable("id")int id) {
//    	
//    	
//    	//System.out.println("Studentname:"+name);
//		return "Studentname:"+name +'\n'+ "Student id:"+id ;
//				
//				             
//    }
    
    @GetMapping("/add")
    //@ResponseBody//query param
    public String studentDetails(@RequestParam(value="Name", defaultValue ="Selva") String Name,String place,Long mobileNum) {
		return  "StudentName:"+" "+Name
				+'\n'+"City:"+" "+place
				+'\n'+"Moblienum:"+" "+mobileNum;
    	
    }
    
    /*
     * String place,Long mobileNum
     * 
     * 
     * +'\n'+"City:"+" "+place
				+'\n'+"Moblienum:"+" "+mobileNum
     */
    //variable and parameter can be different 
    @PostMapping("/year")
    public String studentyear(@RequestParam (name="Year") String Yearofstudy) {
		return "Student POY : "+Yearofstudy;
    	
    }
    
    @GetMapping
    public String StudentProgress(@RequestParam (required =false)String result ) {
		return "Student Result: "+ result;
    	
    	
    }
    
    
}
